<html>
  <head>
    <title>Ajout d'utilisateur</title>
    <link rel="stylesheet" type="text/css" href="style.css">
  </head>
  <body>
    <h1>Ajout d'utilisateur</h1>
    <form action="add_user.php" method="post">
      <label for="nom">Nom :</label>
      <input type="text" name="nom" id="nom"><br><br>
      <label for="cin">CIN :</label>
      <input type="text" name="cin" id="cin"><br><br>
      <label for="age">Age :</label>
      <input type="text" name="age" id="age"><br><br>
      <input type="submit" name="submit" value="Ajouter">
    </form>


    <?php
  require_once('config.php');

  if(isset($_POST['submit'])) {
    $nom = $_POST['nom'];
    $cin = $_POST['cin'];
    $age = $_POST['age'];

    try {
      $query = "INSERT INTO utilisateur (nom, cin, age) VALUES (:nom, :cin, :age)";
      $stmt = $db->prepare($query);
      $stmt->execute(array(':nom' => $nom, ':cin' => $cin, ':age' => $age));
      header("Location: index.php");
    } catch (PDOException $e) {
      echo "Erreur dans la requête : " . $e->getMessage();
    }
  }
?>


  </body>
</html>

