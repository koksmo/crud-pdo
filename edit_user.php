<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Document</title>
</head>
<body>
    
<?php

require_once 'config.php';

if (!empty($_GET['id'])) {
  $id = $_GET['id'];
  $stmt = $db->prepare("SELECT * FROM utilisateur WHERE id = ?");
  if (!$stmt->execute([$id])) {
    echo "Erreur dans la requête : " . implode(", ", $stmt->errorInfo());
    die;
  }
  $row = $stmt->fetch();
  if (!$row) {
    echo "L'utilisateur n'existe pas.";
    die;
  }
  if (!empty($_POST)) {
    $nom = $_POST['nom'];
    $cin = $_POST['cin'];
    $age = $_POST['age'];
    $stmt = $db->prepare("UPDATE utilisateur SET nom = ?, cin = ?, age = ? WHERE id = ?");
    if (!$stmt->execute([$nom, $cin, $age, $id])) {
      echo "Erreur dans la requête : " . implode(", ", $stmt->errorInfo());
      die;
    }
    header("Location: index.php");
  }
} else {
  echo "L'ID de l'utilisateur n'est pas spécifié.";
  die;
}
?>
<form action="edit_user.php?id=<?php echo $id; ?>" method="post">
  <input type="text" name="nom" placeholder="Nom" value="<?php echo $row['nom']; ?>">
  <input type="text" name="cin" placeholder="CIN" value="<?php echo $row['cin']; ?>">
  <input type="number" name="age" placeholder="Age" value="<?php echo $row['age']; ?>">
  <button type="submit">Modifier</button>
</form>




</body>
</html>