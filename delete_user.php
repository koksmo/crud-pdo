<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Document</title>
</head>
<body>
    
<?php

require_once 'config.php';


if (!empty($_GET['id'])) {
  $id = $_GET['id'];
  $stmt = $db->prepare("DELETE FROM utilisateur WHERE id = ?");
  if (!$stmt->execute([$id])) {
    echo "Erreur dans la requête : " . implode(", ", $stmt->errorInfo());
    die;
    }
    header("Location: index.php");
    } else {
    echo "L'ID de l'utilisateur n'est pas spécifié.";
    die;
    }

?>

</body>
</html>