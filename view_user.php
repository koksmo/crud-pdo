<?php
include("config.php");

if (isset($_GET['id'])) {
  $id = $_GET['id'];
  
  $query = "SELECT * FROM utilisateur WHERE id=:id";
  $stmt =  $db->prepare($query);
  $stmt->execute(['id' => $id]);
  
  $user = $stmt->fetch();
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Afficher l'utilisateur</title>
  <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  <div class="container">
    <h1>Informations sur l'utilisateur</h1>
    <table>
      <tr>
        <td>ID</td>
        <td><?php echo $user['id']; ?></td>
      </tr>
      <tr>
        <td>Nom</td>
        <td><?php echo $user['nom']; ?></td>
      </tr>
      <tr>
        <td>CIN</td>
        <td><?php echo $user['cin']; ?></td>
      </tr>
      <tr>
        <td>Age</td>
        <td><?php echo $user['age']; ?></td>
      </tr>
    </table>
    <a href="index.php">Retour à la liste des utilisateurs</a>
  </div>
</body>
</html>
