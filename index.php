
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Document</title>
</head>
<body>
    

<?php
require_once 'config.php';
require_once 'add_user.php';
$stmt = $db->prepare("SELECT * FROM utilisateur");
if (!$stmt->execute()) {
  echo "Erreur dans la requête : " . implode(", ", $stmt->errorInfo());
  die;
}
$rows = $stmt->fetchAll();
?>
<table>
  <thead>
    <tr>
      <th>ID</th>
      <th>Nom</th>
      <th>CIN</th>
      <th>Age</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $row) : ?>
    <tr>
      <td><?php echo $row['id']; ?></td>
      <td><?php echo $row['nom']; ?></td>
      <td><?php echo $row['cin']; ?></td>
      <td><?php echo $row['age']; ?></td>
      <td>
        <a href="view_user.php?id=<?php echo $row['id']; ?>">Voir</a>
        <a href="edit_user.php?id=<?php echo $row['id']; ?>">Modifier</a>
        <a href="delete_user.php?id=<?php echo $row['id']; ?>">Supprimer</a>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>



</body>
</html>
